const { Client } = require('pg');
// Step pertama buat database setelah buat database comment fungsi create database client yang mengarah ke database postgres

const client = new Client({
  host: '127.0.0.1',
  user: 'postgres',
  password: 'rudi',
  port: 5432,
});

// // Fungsi CREATE DATABASE
const createDatabase = async () => {
  try {
    await client.connect();
    await client.query('CREATE DATABASE dbntb');
    return true;
  } catch (error) {
    console.error(error.stack);
    return false;
  } finally {
    await client.end();
  }
};

createDatabase().then((result) => {
  if (result) {
    console.log('Database Berhasil Dibuat');
  }
});

//step setelah membuat database
// const client = new Client({
//   host: '127.0.0.1',
//   user: 'postgres',
//   password: 'rudi',
//   database: 'dbntb',
//   port: 5432,
// });

// // Fungsi crud

// const createTable = async () => {
//   try {
//     await client.connect();

//query crete table
// await client.query('CREATE TABLE provinsi(id_provinsi serial not null primary key, nama varchar(50) not null);');
// await client.query('CREATE TABLE kota(id_kota serial not null primary key, nama varchar(20) not null, id_provinsi serial not null, foreign key (id_provinsi) references provinsi(id_provinsi) );');
// await client.query('CREATE TABLE kabupaten (id_kabupaten serial not null primary key, nama varchar(50) not null, id_kota serial not null, foreign key (id_kota) references kota(id_kota));');

//query select
// await client.query('select * from provinsi;');

// query insert
// await client.query(`insert into provinsi values(1, 'ntb');`);
// await client.query(`insert into kota values(1,'praya',1);`);
// await client.query(`insert into kabupaten values(1,'mataram',1);`);

//query update
// await client.query(`update kabupaten set nama = 'lombok tengah' where nama = 'mataram';`);

//query delete
// await client.query('create table hapus (nama varchar(10) not null);');
// await client.query('drop table hapus;');
//     return true;
//   } catch (error) {
//     console.error(error.stack);
//     return false;
//   } finally {
//     await client.end();
//   }
// };

// createTable().then((result) => {
//   if (result) {
//     console.log('CRUD Berhasil');
//   }
// });
